extends RigidBody2D

func _ready():
	pass

func set_limb(n):
	$Sprite.frame = n

func set_vel(vel):
	linear_velocity = vel

func set_pos(pos):
	position = pos