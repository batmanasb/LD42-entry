extends RigidBody2D

var limb_node = preload("res://Limb.tscn")
var circle_planet = preload("res://CirclePlanet.tscn")
var rectangle_planet = preload("res://RectanglePlanet.tscn")
var mole_planet = preload("res://MolePlanet.tscn")
var water_planet = preload("res://WaterPlanet.tscn")
var fin = preload("res://Fin.tscn")

onready var world = get_tree().get_root().get_node("World")
onready var planet = get_tree().get_root().get_node("World/Planet")
onready var planetary_bodies_top = get_tree().get_root().get_node("World/Planet/PlanetaryBodiesTop")
onready var planetary_bodies_bot = get_tree().get_root().get_node("World/Planet/PlanetaryBodiesBot")
onready var camera = get_tree().get_root().get_node("World/Camera2D")
onready var label = get_tree().get_root().get_node("World/Runner/Label")
onready var anim = get_tree().get_root().get_node("World/Runner/AnimationPlayer")
onready var sprite = get_tree().get_root().get_node("World/Runner/Sprite")
onready var goal = get_tree().get_root().get_node("World/Goal")

var level = 0
var target_number = 0

var speed = 200
var vert_vel = 0
var is_in_orbit = false
var is_alive = true
var is_facing_right = true

var HUD_update_rate = 0.1
var HUD_update_cooldown = 0.0
var win_timer = 1.0
var win_cooldown = win_timer

# spawn protection to prevent a weird double kill bug where you die when resetting
var spawn_timer = 0.5
var spawn_cooldown = spawn_timer
var respawning = false

var initial_pos = null
var initial_rot = null
var initial_zoom = null

func _ready():
	set_physics_process(true)
	set_process(true)
	
	anim.play("idle_right")
	
	initial_pos = position
	initial_rot = sprite.rotation
	initial_zoom = camera.zoom
	
	set_planet(level)

func _physics_process(delta):
	if(is_alive):
		# movement
		var vel = Vector2()
		if(Input.is_action_pressed("run_left")):
			vel.y += 1
			is_facing_right = false
		if(Input.is_action_pressed("run_right")):
			vel.y -= 1
			is_facing_right = true
		if(vel.y < 0.1 and vel.y > -0.1):
			if(anim.current_animation != "idle_left" and anim.current_animation != "idle_right"):
				if(is_facing_right):
					anim.play("idle_right")
				else:
					anim.play("idle_left")
		elif(vel.y < -0.1):
			if(anim.current_animation != "run_right"):
				anim.play("run_right")
		elif(vel.y > 0.1):
			if(anim.current_animation != "run_left"):
				anim.play("run_left")
		if(is_in_orbit):
			apply_impulse(Vector2(), vel.rotated(get_angle_to(planet.position) + deg2rad(0)) * speed * delta)
		else:
			apply_impulse(Vector2(), vel.rotated(deg2rad(90))*speed*delta)
	
	# keep tract of the vertical velocity
	vert_vel = linear_velocity.rotated(-get_angle_to(planet.position)).x
	
	# manage wind noise
	if($WindNoisePlayer.playing):
		if(linear_velocity.length() < 100):
			$WindNoisePlayer.stop()
	else:
		if(is_alive and is_in_orbit and linear_velocity.length() > 300):
			$WindNoisePlayer.play()
	
func _integrate_forces(state):
	if(respawning):
		linear_velocity = Vector2()
	
	if(state.get_contact_count() > 0):
		# calculate how fast the runner collided with the ground
		var normal = state.get_contact_local_normal(0)
		var impact = linear_velocity.rotated(-normal.angle()).x
		
		# too hard impacts cause death
		if(is_alive):
			if(impact > 50):
				kill()
	
func _process(delta):
	#rotate the sprite in relation to the planet
	if(is_in_orbit):
		sprite.rotation = get_angle_to(planet.position) + deg2rad(-90)
	
	# respawn protection cooldown
	if(respawning):
		spawn_cooldown -= delta
		if(spawn_cooldown < 0):
			respawning = false
	
	# retry level button
	if(Input.is_action_just_pressed("retry")):
		if(is_alive):
			kill()
		reset_runner()
	
	if(HUD_update_cooldown < 0):
		var vel = int(linear_velocity.length())
		
		# making the angle look pretty
		var angle = -int(rad2deg(linear_velocity.angle() - get_angle_to(planet.position))) + 180
		if(angle >= 180):
			angle -= 360
		angle = 180 + angle
		if(angle >= 180):
			angle -= 360
		
		HUD_update_cooldown = HUD_update_rate
		label.set_text(str(vel," MPH\n", angle,"°\n", int(vert_vel), " MPH Downward"))
	else:
		HUD_update_cooldown -= delta
	
	# check if the runner is standing next to the goal, if so, move onto the next challenge
	if(is_alive and linear_velocity.length() < 10.0 and position.distance_to(goal.position) < 55.0):
		if(win_cooldown < 0):
			set_planet(level)
			reset_runner()
		else:
			win_cooldown -= delta

func _on_Gravity_body_entered(body):
	if(body.get_name() == "Runner"):
		is_in_orbit = true
		$WindNoisePlayer.play()

func _on_Gravity_body_exited(body):
	if(body.get_name() == "Runner"):
		is_in_orbit = false
		$WindNoisePlayer.stop()

func reset_runner():
	# reset all runner related settings
	position = initial_pos
	sprite.rotation = initial_rot
	camera.zoom = initial_zoom
	camera.zoom_out_cooldown = camera.zoom_out_timer
	is_in_orbit = false
	$WindNoisePlayer.stop()
	is_alive = true
	is_facing_right = true
	linear_velocity = Vector2()
	anim.play("idle_right")
	win_cooldown = win_timer
	spawn_cooldown = spawn_timer
	respawning = true
	mode = MODE_CHARACTER
	set_collision_layer_bit(0, 1)
	set_collision_mask_bit(0, 1)

func kill():
	if(not respawning):
		is_alive = false
		
		# spawn limbs
		for i in range(3, -1, -1):
			var new_node = limb_node.instance()
			new_node.set_limb(i)
			new_node.set_vel(linear_velocity)
			new_node.set_pos(position)
			planetary_bodies_top.add_child(new_node)
		
		# basically disable the runner
		anim.play("dead")
		mode = MODE_STATIC
		set_collision_layer_bit(0, 0)
		set_collision_mask_bit(0, 0)
		$WindNoisePlayer.stop()
		$SplatNoisePlayer.play()
		
		# move the blood splater onto whatever was collided with
		sprite.position = Vector2(0,35).rotated(sprite.rotation)

func set_planet(n):
	# clear current planetary bodies (only clear top (limbs) if new level)
	if(target_number == 0):
		for i in range(planetary_bodies_top.get_child_count() - 1, -1, -1):
			planetary_bodies_top.get_child(i).queue_free()
	for i in range(planetary_bodies_bot.get_child_count() - 1, -1, -1):
		planetary_bodies_bot.get_child(i).queue_free()
	
	# spawn new planetary bodies
	var node = null
	if(n == 0):
		node = circle_planet.instance()
	elif(n == 1):
		node = rectangle_planet.instance()
	elif(n == 2):
		node = water_planet.instance()
	elif(n == 3):
		node = mole_planet.instance()
	elif(n >= 4):
		node = fin.instance()
	planetary_bodies_bot.add_child(node)
	
	target_number += 1
	if(node.has_node(str("Target",target_number))):
		# set goal
		var target = node.get_node(str("Target",target_number))
		goal.rotation = target.rotation
		goal.position = target.position
	else:
		level += 1
		target_number = 0
		set_planet(level)
	