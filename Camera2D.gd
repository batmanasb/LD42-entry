extends Camera2D

onready var runner = get_tree().get_root().get_node("World/Runner")
onready var planet = get_tree().get_root().get_node("World/Planet")

var initial_zoom_scale = 8
var initial_distance = null
var initial_pos = null

var zoom_out_timer = 7.0
var zoom_out_cooldown = zoom_out_timer

func _ready():
	set_physics_process(true)
	#initial_zoom_scale = zoom.x
	initial_distance = runner.position.distance_to(planet.position)
	initial_pos = position

func _physics_process(delta):
	var distance = runner.position.distance_to(planet.position)
	var zoom_scale = distance/initial_distance*initial_zoom_scale
	var middle_position = (runner.position - planet.position) / 2
	
	if(runner.is_in_orbit):
		if(zoom_out_cooldown < 0):
			zoom = Vector2(zoom_scale, zoom_scale)
			position = middle_position
		else:
			zoom_out_cooldown -= delta
			var rate = (1 - zoom_out_cooldown / zoom_out_timer)
			var zoom_sub_scale = max(0.5, zoom_scale * rate)
			zoom = Vector2(zoom_sub_scale, zoom_sub_scale)
			position.x = lerp(runner.position.x, middle_position.x, rate)
			position.y = lerp(runner.position.y, middle_position.y, rate)
	else:
		position = runner.position
