extends AnimationPlayer

var dialog = null

var time = 0.0
var phase = 0
var moment = 8
var step = 3

func _ready():
	play("intro")
	set_process(true)
	dialog = get_tree().get_root().get_node("Intro/Dialog").get_children()

func _process(delta):
	time += delta
	if(phase == 0 and time > moment):
		dialog[0].show()
		phase += 1
		moment += step
	elif(phase == 1 and time > moment):
		dialog[phase].show()
		dialog[phase-1].hide()
		phase += 1
		moment += step
	elif(phase == 2 and time > moment):
		dialog[phase].show()
		dialog[phase-1].hide()
		phase += 1
		moment += step
	elif(phase == 3 and time > moment):
		dialog[phase].show()
		dialog[phase-1].hide()
		phase += 1
		moment += step
	elif(phase == 4 and time > moment):
		dialog[phase].show()
		dialog[phase-1].hide()
		phase += 1
		moment += step
	elif(phase == 5 and time > moment):
		dialog[phase].show()
		dialog[phase-1].hide()
		phase += 1
		moment += step
	elif(phase == 6 and time > moment):
		dialog[phase].show()
		phase += 1
		moment += step
	elif(phase == 7 and time > moment):
		dialog[phase].show()
		dialog[phase-1].hide()
		dialog[phase-2].hide()
		phase += 1
		moment += step
	elif(phase == 8 and time > moment):
		dialog[phase].show()
		dialog[phase-1].hide()
		phase += 1
		moment += step
	elif(phase == 9 and time > moment):
		dialog[phase].show()
		dialog[phase-1].hide()
		phase += 1
		moment += step
	elif(phase == 10 and time > moment):
		get_tree().change_scene("res://Main.tscn")
